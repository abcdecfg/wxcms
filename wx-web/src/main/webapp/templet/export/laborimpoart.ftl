<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
</head>

<body>
<table border="1">
    <tr>
        <th>姓名</th>
        <th>身份证号</th>
        <th>联系方式</th>
        <th>市州</th>
        <th>区县</th>
        <th>工种类别</th>
        <th>具体工种</th>
        <th>录入日期</th>
    </tr>
    <!--判读是否有值-->
<#if list??&&((list?size)>0)>
 <#list list as data>
     <tr>
         <td>${data.user_name!''}</td>
         <td>${data.user_idcard!''}</td>
         <td>${data.user_phone!''}</td>
         <td>${data.city_name!''}</td>
         <td>${data.district_name!''}</td>
         <td>${data.work_name!''}</td>
         <td>${data.work_remark!''}</td>
         <td>${data.create_time!''}</td>
     </tr>
 </#list>
</#if>
</table>
</body>
</html>